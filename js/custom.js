


$(document).ready(function(){

	/*
  ===============================================
  CALL STICKY PLUGIN TO MAKE HEADER FIXED
  ===============================================
  */

    $("#sticker").sticky({topSpacing:0});

    /*
  ===============================================
  END OF CALL STICKY PLUGIN TO MAKE HEADER FIXED
  ===============================================
  */

  	/*
  ===============================================
  Code FOR MAKE SLIDER OF TEAM MEMBERS IN MOBILE VERSION
  ===============================================
  */

  if ($(window).width() <= 768) {

   	    $("#owl-team").addClass('owl-carousel');

   	    $("#owl-team").owlCarousel({
		 
		    // Most important owl features
		    // items : 4,
		    // itemsDesktop: [1200,4],
		    // itemsDesktopSmall: [979,4],
		    // itemsTablet: [768,1],
		    singleItem:true,
		    navigation : false,
		    pagination: true

		  });
	   
	}


		/*
  ===============================================
  END OF Code FOR MAKE SLIDER OF TEAM MEMBERS IN MOBILE VERSION
  ===============================================
  */


  /*
  ===============================================
  COde Counter down
  ===============================================
  */
	function countdown(){
		var now = new Date();
		var eventDate = new Date(2018, 0, 1);

		var currentTiime = now.getTime();
		var eventTime = eventDate.getTime();

		var remTime = eventTime - currentTiime;

		var s = Math.floor(remTime / 1000);
		var m = Math.floor(s / 60);
		var h = Math.floor(m / 60);
		var d = Math.floor(h / 24);

		h %= 24;
		m %= 60;
		s %= 60;

		h = (h < 10) ? "0" + h : h;
		m = (m < 10) ? "0" + m : m;
		s = (s < 10) ? "0" + s : s;

		document.getElementById("days").textContent = d;
		document.getElementById("days").innerText = d;

		document.getElementById("hours").textContent = h;
		document.getElementById("minutes").textContent = m;
		document.getElementById("seconds").textContent = s;

		setTimeout(countdown, 1000);
	}

	countdown();

	/*
  ===============================================
  END OF COde Counter down
  ===============================================
  */
 

});